package de.sme.azubi.state.machine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class StateManagerTest {

	private IStateManager testController;

	@Mock
	private IStateMachineListener listener;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		testController = null;// TODO init testController with an implementation
								// of IStateManager
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testPerformCommand() {
		testController.init();

		for (Commands command : Commands.values()) {
			testController.performCommand(command, null);
			testGetStatus();
			testController.performCommand(command, new Object());
			testGetStatus();
			testController.performCommand(command, new Object[2]);
			testGetStatus();
		}
	}

	@Test
	public final void testGetStatus() {
		assertNotNull(testController.getStatus());
		assertTrue(testController.getStatus().trim().length() > 0);
	}

	@Test
	public final void testInit() {
		assertFalse(testController.isActive());
		testController.init();
		assertFalse(testController.isActive());
	}

	@Test
	public final void testReset() {
		assertTrue(testController.isActive());
		testController.reset();
		assertFalse(testController.isActive());
	}

	@Test
	public final void testClose() {
		if (testController.isActive()) {
			testController.close();
			assertFalse(testController.isActive());
		} else {
			testController.reset();
			assertFalse(testController.isActive());

			testController.close();
			assertFalse(testController.isActive());
		}
	}

	@Test
	public final void testAddStateMachineListener() {
		testController.addStateMachineListener(listener);
		testController.addStateMachineListener(listener);
		testController.addStateMachineListener(listener);

		Object[] options = null;

		testController.performCommand(Commands.PUSH, options);

		Mockito.verify(listener).onCommandPerformed(options);
	}

	@Test
	public final void testRemoveStateMachineListener() {
		testAddStateMachineListener();

		testController.removeStateMachineListener(listener);

		Object option = new Object();

		Mockito.doThrow(Exception.class).when(listener)
				.onCommandPerformed(option);

		testController.performCommand(Commands.PUSH, option);
	}

}
