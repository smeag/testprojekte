package de.sme.azubi.state.machine;

public interface IStateMachineListener {

	public void onCommandPerformed(Object... info);
}
