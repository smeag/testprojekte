package de.sme.azubi.state.machine;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public abstract class AbstractState {

	public void doit(){	
		Map<Integer,Integer> mapA = null;
		Map<Integer,Integer> mapB = null;
		
		Iterator<Entry<Integer,Integer>> it = mapA.entrySet().iterator();
		
		while(it.hasNext()){
			Entry<Integer,Integer> entry = it.next();
			
			Integer calcBooks = entry.getKey();
			Integer books1 = entry.getValue();
			
			if(mapB.containsKey(calcBooks)){
				
				Integer books2 = mapB.get(calcBooks);
				
				if(books1.equals(books2)){
					mapB.remove(entry);
					it.remove();
				}
			}
		}
	}
}
