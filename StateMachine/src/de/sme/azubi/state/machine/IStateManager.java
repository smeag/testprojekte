package de.sme.azubi.state.machine;

public interface IStateManager {

	public void performCommand(Commands command, Object... options);
	
	public String getStatus();
	
	public void init();
	
	public boolean isActive();
	
	public void reset();
	
	public void close();
	
	public void addStateMachineListener(IStateMachineListener l);
	
	public boolean removeStateMachineListener(IStateMachineListener l);
}
