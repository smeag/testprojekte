package de.sme.azubi.state.machine;

public enum Commands {

	PUSH, POLL, SEEK_FORWARD, SEEK_BACKWARD;
}
